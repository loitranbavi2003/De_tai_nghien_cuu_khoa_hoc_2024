#include "led.h"
#include "stm32f10x.h"


void led_init(void)
{
	GPIO_InitTypeDef Led;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	Led.GPIO_Mode = GPIO_Mode_Out_PP;
	Led.GPIO_Pin = GPIO_CONTROL_RS485;
	Led.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(PORT_CONTROL_RS485, &Led);
}
